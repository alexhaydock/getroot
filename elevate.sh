#!/bin/sh

if [ -n "$HOSTUID" ]; then
  USER="$(grep $HOSTUID /host/etc/passwd | cut -d":" -f1)"
  echo "Assuming your username is $USER based on host UID of $HOSTUID."
else
  USER="$(grep 1000 /host/etc/passwd | cut -d":" -f1)"
  echo "UID not passed as environment variable from host. Assuming your username is $USER."
fi

if [ "$(grep $USER /host/etc/sudoers | wc -l)" -gt 0 ]; then
  echo "It looks like you are already in the sudoers file."
  echo "Taking no action."
else
  echo "I'm about to add $USER to the /etc/sudoers file."
  echo "If you don't want to do this, press Ctrl^C in the next 5 seconds..."
  sleep 5
  echo "$USER ALL=(ALL:ALL) ALL" >> /host/etc/sudoers
  visudo -cf /host/etc/sudoers && echo "I have added you to the sudoers file. Now try to use sudo."
fi
