FROM alpine:3
LABEL maintainer "Alex Haydock <alex@alexhaydock.co.uk>"
LABEL name "getroot"
LABEL version "1.1"

RUN apk add --no-cache sudo
COPY elevate.sh /elevate.sh
RUN chmod +x elevate.sh

CMD /elevate.sh
