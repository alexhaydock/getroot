ARCH = $(shell uname -m)

build:
	docker build -t registry.gitlab.com/alexhaydock/getroot:$(ARCH) .
	docker push registry.gitlab.com/alexhaydock/getroot:$(ARCH)
